import sqlhelp


def print_list_employees(print_list_employees):
    print("ID, Name, Password, Email, Salary")
    for x in print_list_employees:
        print(x[0], x[1], x[2], x[3], x[4])
    print("\n")


def print_all_employees():
    employees = sqlhelp.get_all_employees()
    if not employees:
        print("Brak pracowników")
    else:
        print_list_employees(employees)

    menu()


def print_employees_by_id():
    id_employee = int(input("podaj ID praconika którego chcesz wyszukać "))
    employees = sqlhelp.get_employees_by_id(id_employee)

    if not employees:
        print("Nie ma pracownika o podanym id")
    else:
        print_list_employees(employees)
    menu()


def print_employees_by_email():
    email = str(input("podaj email pracownika "))
    employees = sqlhelp.get_employees_by_email(email)

    if not employees:
        print("Nie pracownika o podanym email")
    else:
        print_list_employees(employees)
    menu()


def print_employees_by_salary():
    salary = int(input("podaj wynagrodzenie "))
    employees = sqlhelp.get_employees_by_salary(salary)

    if not employees:
        print("Nie ma pracownika który zarabia więcej niż " + str(salary))
    else:
        print_list_employees(employees)
    menu()


def add_employee():
    name = input("Podaj imię pracownika ")
    password = input("Podaj hasło pracownika ")
    email = input("Podaj email pracownika ")
    salary = input("Podaj wynagrodzenie pracownika")
    sqlhelp.add_employee(name, password, email, salary)
    menu()


def menu():
    print("""
1. Pobierz wszystkich pracowników
2. Pobierz pracownika z id
3. Pobierz pracownika z email
4. Pobierz pracownika z zarobkami większymi niż
5. Dodaj pracownika
6. Wyjdz""")
    choice = int(input())

    if choice == 1:
        print_all_employees()
    if choice == 2:
        print_employees_by_id()
    if choice == 3:
        print_employees_by_email()
    if choice == 4:
        print_employees_by_salary()
    if choice == 5:
        add_employee()


menu()
