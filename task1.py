import sqlhelp


def menu():
    print("""
1. Logowanie
2. Rejestracja
3. Lista użytkowników bez haseł
4. zmien hasło
5. usun konto
6. Wyjdz""")

    choice = int(input())
    if choice == 1:
        login()
        menu()
    if choice == 2:
        register()
    if choice == 3:
        show_list_users()
    if choice == 4:
        change_password()
    if choice == 5:
        remove_user()


def login():
    email = input("Podaj adres email ")
    password = input("Podaj hasło ")
    if sqlhelp.login(email, password):
        print("Witaj " + email + "\nZalogowałeś się poprawnie")
    else:
        print("podany login lub hasło jest nieprawidłowe")


def register():
    email = input("Podaj adres email ")
    password1 = input("Podaj hasło ")
    password2 = input("Podaj hasło ponownie ")
    if password1 != password2:
        print("podałeś niepoprane hasła\nSpróbuj jeszcze raz")
        register()
    else:
        sqlhelp.add_user(email, password1)
        menu()


def show_list_users():
    users = sqlhelp.get_all_users()
    if not users:
        print("Brak użytkowników")
    else:
        print_list_users(users)

    menu()


def print_list_users(list_users):
    print("Email")
    for x in list_users:
        print(x[0], x[1])
    print("\n")


def change_password():
    email = input("Podaj adres email ")
    password = input("Podaj hasło ")
    if sqlhelp.login(email, password):
        print("Witaj " + email + "\nZalogowałeś się poprawnie")
        new_password1 = input("Podaj nowe hasło ")
        new_password2 = input("Podaj nowe hasło ponownie ")
        if new_password1 != new_password2:
            print("podałeś niepoprane hasła\nSpróbuj jeszcze raz")
            change_password()
        else:
            sqlhelp.change_pass_user(email, new_password1)
            print("Hasło zostało zmienione prawidłowo")
            menu()
    else:
        print("podany login lub hasło jest nieprawidłowe")
        change_password()


def remove_user():
    email = input("Podaj adres email ")
    password = input("Podaj hasło ")
    if sqlhelp.login(email, password):
        print("Witaj " + email + "\nZalogowałeś się poprawnie")
        sqlhelp.remove_user(email)
        print("konto" + email + "\nZostało usunięte")
    menu()


menu()
