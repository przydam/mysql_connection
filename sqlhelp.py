import hashlib

import mysql.connector


def get_all_employees():
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="pass",
        database="baza"
    )
    mycursor = mydb.cursor()
    mycursor.execute("SELECT * FROM employee")
    return mycursor.fetchall()


def get_all_users():
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="pass",
        database="baza"
    )
    mycursor = mydb.cursor()
    mycursor.execute("SELECT email FROM users")
    return mycursor.fetchall()


def get_employees_by_id(id_employee):
    result_list = []
    myresult = get_all_employees()
    for x in myresult:
        if x[0] == id_employee:
            result_list.append(x)
    return result_list


# mycursor.execute("SELECT * FROM employee"+" WHERE id ="+str(id_employee))
# mycursor.execute(f"SELECT * FROM employee WHERE id = {id_employee})

def get_employees_by_email(email):
    result_list = []
    myresult = get_all_employees()
    for x in myresult:
        if x[3] == email:
            result_list.append(x)
    return result_list


# mycursor.execute(f"SELECT * FROM employee WHERE email LIKE \"{email}\"")

def get_employees_by_salary(salary):
    result_list = []
    myresult = get_all_employees()
    for x in myresult:
        if x[4] > salary:
            result_list.append(x)
    return result_list


# mycursor.execute(f"SELECT * FROM employee WHERE salary > {salary})
def add_employee(name, password, email, salary):
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="pass",
        database="baza"
    )
    mycursor = mydb.cursor()
    sql = "INSERT INTO employee (name, password, email, salary) VALUES (%s, %s, %s, %s)"
    value = (name, password, email, str(salary))
    mycursor.execute(sql, value)
    mydb.commit()


def add_user(email, password):
    pass_hash = hashlib.sha3_256(password.encode('utf-8')).hexdigest()
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="pass",
        database="baza"
    )
    mycursor = mydb.cursor()
    sql = "INSERT INTO users (email, password) VALUES (%s, %s)"
    value = (email, pass_hash)
    mycursor.execute(sql, value)
    mydb.commit()


def login(email, password):
    pass_hash = hashlib.sha3_256(password.encode('utf-8')).hexdigest()
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="pass",
        database="baza"
    )
    mycursor = mydb.cursor()
    mycursor.execute(f"SELECT * FROM users WHERE email LIKE \"{email}\" and password LIKE \"{pass_hash}\"")
    myresult = mycursor.fetchall()

    if not myresult:
        return False
    else:
        return True


def get_all_users():
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="pass",
        database="baza"
    )
    mycursor = mydb.cursor()
    mycursor.execute("SELECT * FROM users")
    return mycursor.fetchall()


def change_pass_user(email, new_password1):
    pass_hash = hashlib.sha3_256(new_password1.encode('utf-8')).hexdigest()
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="pass",
        database="baza"
    )
    mycursor = mydb.cursor()

    sql = "UPDATE users SET password = %s WHERE email = %s"
    #    sql = f"Update users SET password = '{pass_hash}' WHERE email LIKE '{email}'"
    val = (pass_hash, email)
    mycursor.execute(sql, val)

    mydb.commit()


def remove_user(email):
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="pass",
        database="baza"
    )
    mycursor = mydb.cursor()

    sql = f"DELETE FROM users WHERE email LIKE '{email}'"
    mycursor.execute(sql)
    mydb.commit()

#    sql = "DELETE FROM `users` WHERE email = %s"
#    mycursor.execute(sql, (email,))
#    mydb.commit()


#    print(mycursor.rowcount, "record(s) deleted")
